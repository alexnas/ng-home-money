# Angular Home Money

This Angular application created for home accounting and finance tracking.

## Uses

- Angular 7.2
- Angular Router
- Angular Forms
- Rxjs
- D3
- JsonServer through API
- Login/password authentication/registering through API (JsonServer)

## Installation and Usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm server` to start the JsonServer at the development.
- Run `npm dev` to start the project at the development.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

- Open app at localhost:4200 in a browser

## License

Under the terms of the MIT license.
